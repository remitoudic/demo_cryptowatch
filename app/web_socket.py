import cryptowatch as cw
import time


class retieve_data:
    def __init__(self):

        # Set your API Key
        cw.api_key = "FRIK08BC5697ITHM0Q6S"

        # Subscribe to resources (https://docs.cryptowat.ch/websocket-api/data-subscriptions#resources)
        cw.stream.subscriptions = ["markets:*:trades"]

        self.data = {"data": []}

    def handle_trades_update(self, trade_update):
        """
        # What to do on each trade update
        trade_update follows Cryptowatch protocol buffer format:
        https://github.com/cryptowatch/proto/blob/master/public/markets/market.proto
        """
        # print("Debug", trade_update.marketUpdate.market)

        market_msg = {
            "marketId": trade_update.marketUpdate.market.marketId,
            "exchangeId": trade_update.marketUpdate.market.exchangeId,
            "currencyPairId": trade_update.marketUpdate.market.currencyPairId,
            "len _ trades": len(trade_update.marketUpdate.tradesUpdate.trades),
            "trades": trade_update.marketUpdate.tradesUpdate.trades,
        }

        # for trade in trade_update.marketUpdate.tradesUpdate.trades:
        #     trade_msg = "\tID:{} TIMESTAMP:{} TIMESTAMPNANO:{} PRICE:{} AMOUNT:{}".format(
        #         trade.externalId,
        #         trade.timestamp,
        #         trade.timestampNano,
        #         trade.priceStr,
        #         trade.amountStr,
        #     )
        self.data["data"].append(market_msg)

    def stream_data(self):
        cw.stream.on_trades_update = self.handle_trades_update

        # Start receiving
        cw.stream.connect()

        time.sleep(5)

        # Call disconnect to close the stream connection
        cw.stream.disconnect()

        return self.data


if __name__ == "__main__":

    test = retieve_data()
    test.stream_data()
    print(test.data)
