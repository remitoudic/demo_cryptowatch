import uvicorn
from fastapi import FastAPI
import app.web_socket as web_socket
import time

app = FastAPI()


@app.get("/")
async def root():
    test = web_socket.retieve_data()
    test.stream_data()
    new = str(test.data["data"][0:5])
    return {"test": new}


# if __name__ == "__main__":
#     uvicorn.run("app:app", port=5000, host="127.0.0.1")
